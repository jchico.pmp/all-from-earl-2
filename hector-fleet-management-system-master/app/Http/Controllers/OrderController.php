<?php

namespace App\Http\Controllers;

use App\Order;
use App\Vehicle_model;
use App\Vehicle;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Order $order)
    {
        $this->authorize('viewAny', $order);

        return view('orders.index')->with('orders', Order::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if(Auth::user())

        return view('orders.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Session::get('order_request')) {
            $request->session()->flash('fail', 'There are no veicles requested');

            return redirect()->back();
        }

        $borrow_date = $request->borrow_date;

        $request->validate([
            'borrow_date' => 'required|after_or_equal:' . date('F d, Y'),
            'return_date' => 'required|after_or_equal:' . date('F d, Y'), $request->borrow_date,
            'purpose' => 'required'
        ]);


        // Create order
        $order = new Order;
        $order->order_number = Auth::user()->id . "-" . time() . "-" . rand(pow(10, 2), pow(10, 5)-1);
        $order->user_id = Auth::user()->id;
        $order->borrow_date = $request->input('borrow_date');
        $order->return_date = $request->input('return_date');
        $order->purpose = $request->purpose;

        $order->save();

        // Set vehicle models
        $request_model_ids = array_keys(Session::get('order_request'));
        $request_models = Vehicle_model::find($request_model_ids);

        // Add models to order
        foreach($request_models as $model) {
            $model->quantity = Session::get("order_request.$model->id");
            $order->vehicle_models()->attach($model->id,
                [
                    'quantity' => $model->quantity
                ]);
        }

        $order->save();

        $request->session()->forget("order_request");

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order, Request $request)
    {
        $this->authorize('view', $order);

        $requested_models = [];
        foreach($order->vehicle_models->all() as $vehicle_model) {
            array_push($requested_models, $vehicle_model->id);
        }

        $assigned_vehicles = [];
        foreach($order->vehicles as $vehicle) {
            array_push($assigned_vehicles, $vehicle->id);
        }

        $vehicles = Vehicle::whereIn('vehicle_model_id', $requested_models)
            ->whereNotIn('id', $assigned_vehicles)
            ->get();

        $available_vehicles = [];

        foreach($vehicles as $vehicle) {
            $is_available = true;
            if($vehicle->orders->first()) {
                foreach($vehicle->orders as $vehicle_order) {
                    if($vehicle_order->borrow_date < $order->borrow_date
                        && $vehicle_order->return_date > $order->borrow_date
                        || $vehicle_order->borrow_date < $order->return_date
                        && $vehicle_order->return_date > $order->return_date) {
                        
                        $is_available = false;
                        break;
                    }
                }
            }
            if($is_available) {
                $available_vehicles[] = $vehicle;
            }
        }

        return view('orders.show')->with('order', $order)
            ->with('vehicles', $available_vehicles)->with('statuses', DB::table('order_statuses')->where('id', '<', 4)->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        if($request->query('delete')) {
            $order->vehicle_models()->wherePivot('vehicle_model_id', $request->query('delete'))->detach();

            $request->session()->flash('success', "Successfully removed model from request");

        } elseif($request->query('updateQuantity')) {
            $request->validate([
                'quantity' => 'required|min:1'
            ]);

            DB::table('order_vehicle_model')
                ->where('vehicle_model_id', $request->query('updateQuantity'))
                ->update(['quantity' => $request->quantity]);

            $request->session()->flash('success', "Successfully removed model from request");

        } elseif($request->query('assign')) {
            if($order->vehicles->where('id', $request->input('assign'))->first()) {
                $request->session()->flash('fail', 'Vehicle already assigned in the order');
                // dd($order->vehicles()->get());
            } else {
                $order->vehicles()->attach($request->input('assign'));
                $request->session()->flash('success', 'Vehicle assigned to order');
                // dd($order->vehicles()->get());
            }
            
        } elseif($request->query('unassign')) {
            $order->vehicles()->detach($request->unassign);
            $request->session()->flash('success', 'Vehicle unassigned from order');
            
        } elseif($request->reject !== null) {
            $order->order_status_id = 4;
            $order->save();
        } else {

            if(Auth::user()->can('isUser') && $order->status_id !== 2) {
                $request->validate([
                    'borrow_date' => 'required',
                    'return_date' => 'required|after_or_equal:' .  $request->borrow_date,
                    'purpose' => 'required'
                ]);
                $order->purpose = $request->purpose;
                $order->borrow_date = $request->input('borrow_date');
                $order->return_date = $request->input('return_date');
                
            }

            $order->order_status_id = $request->status;
            $order->save();
        }

        return redirect(route('orders.show', ['order'=> $order->id]))->with('user', Auth::user());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Order $order)
    {
        $order->delete();
        $request->session()->flash('success', 'Order successfully deleted');

        return redirect(route('orders.index'));
    }
}
