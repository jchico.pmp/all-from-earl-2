<?php

namespace App\Http\Controllers;

use App\Vehicle_model;
use App\Vehicle;
use App\Vehicle_category;

use Session;
use Illuminate\Support\Facades\Auth;


use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class VehicleModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
public function index()
    {

        if(Session::has('order_request')) {
            // Get model ids from request
            $request_model_ids = array_keys(Session::get('order_request'));
            
            // Query
            $request_models = Vehicle_model::find($request_model_ids);
            
            // Set quantities
            foreach($request_models as $request_model) {
                $request_model->quantity = Session::get("order_request.$request_model->id");
            }
            // return dd($request_models);

            // Return to index with updated request
            return view('vehicle_models.index')
                ->with('vehicle_models', Vehicle_model::orderBy('make')->paginate(12))
                ->with('request_models', $request_models);
        } else {
            // Return to models index
            return view('vehicle_models.index')->with('vehicle_models', Vehicle_model::orderBy('make')->paginate(12));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vehicle_models.create')->with('categories', Vehicle_category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Vehicle_model $vehicle_model)
    {
        
        $request->validate([
            'name' => 'required',
            'make' => 'required',
            'category' => 'required',
            'year' => 'digits:4',
            'image' => 'image|max:5000',
            'description' => 'required',
            'seats' => 'required|min:1'
            ]);
            
        if(Vehicle_model::where([
            ['make', "=", $request->input('make')],
            ['name', '=', $request->input('name')],
            ['year', '=', $request->input('year')]
        ])->first()) {
            return back()->withErrors('Vehicle model is already existing')->withInput();
        };

        $vehicle_model = new Vehicle_model;
        $vehicle_model->name = ucwords($request->input('name'));
        $vehicle_model->make = ucwords($request->input('make'));
        $vehicle_model->year = $request->input('year');
        $vehicle_model->description = $request->input('description');
        $vehicle_model->seats = $request->input('seats');
        $vehicle_model->vehicle_category_id = $request->input('category');
        $vehicle_model->save();        

        if($request->hasFile('image')) {
            $filename = 'model_' . $vehicle_model->id . "." . $request->image->extension();
            // $vehicle_model->image = $request->image->storeAs('model_images', $filename, 'public');
            $vehicle_model->image = $request->image->storeAs('model-images', $filename, 'public');
    
            $vehicle_model->save();
        }

        $request->session()->flash('success', 'Vehicle successfully added');

        return redirect(route('vehicle_models.show', ['vehicle_model' => $vehicle_model->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle_model $vehicle_model)
    {
        $vehicles = Arr::sort(Vehicle::all()->where('vehicle_model_id', $vehicle_model->id), function ($value) {
            return $value->vehicle_status_id;
        });
        return view('vehicle_models.show')->with('vehicle_model', $vehicle_model)->with('vehicles', $vehicles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle_model $vehicle_model)
    {
        return view('vehicle_models.edit')->with('vehicle_model', $vehicle_model)->with('vehicle_categories', Vehicle_category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle_model $vehicle_model)
    {
        if($vehicle_model->name !== $request->input('name') && $vehicle_model->make !== $request->input('make') && $vehicle_model->year !== $request->input('year')) {
            $request->validate([
                'name' => 'required',
                'make' => 'required',
                'category' => 'required',
                'year' => 'digits:4',
                'description' => 'required',
                'seats' => 'required|min:1'
                ]);
                
            if(Vehicle_model::where([
                ['make', "=", $request->input('make')],
                ['name', '=', $request->input('name')],
                ['year', '=', $request->input('year')]
            ])->first()) {
                return back()->withErrors('Vehicle model is already existing')->withInput();
            };
        } else {
            $request->validate([
                'name' => 'required',
                'make' => 'required',
                'category' => 'required',
                'year' => 'digits:4',
                'description' => 'required',
                'seats' => 'required|min:1'
                ]);
        }

        $vehicle_model->name = $request->input('name');
        $vehicle_model->make = $request->input('make');
        $vehicle_model->year = $request->input('year');
        $vehicle_model->vehicle_category_id = $request->input('category');
        $vehicle_model->description = $request->input('description');
        $vehicle_model->seats = $request->input('seats');

        if($request->has('image')) {
            $request-> validate([
                'image' => 'image|max:5000',
            ]);

            $vehicle_model->image = $request->image->storeAs('/', $vehicle_model->image, 'public');
        }

        $vehicle_model->save();

        return redirect(route('vehicle_models.edit', ['vehicle_model' => $vehicle_model->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle_model  $vehicle_model
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle_model $vehicle_model, Request $request)
    {
        if(!Vehicle::where('vehicle_model_id', $vehicle_model->id)->first()) {
            $vehicle_model->delete();
            $request->session()->flash('success', 'Vehicle model deleted successfully');
        } else {
            $request->session()->flash('fail', 'Unable to remove vehicle model. There are existing vehicles with that model');
        }
        
        return redirect(route('vehicle_models.index'));
    }
}
